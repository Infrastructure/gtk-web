---
---

# GLib

GLib is a general-purpose, portable utility library, which provides many
useful generic data types, macros, type conversions, string utilities, file
utilities, a main loop abstraction, and so on.

Additionally, GLib provides a run time type system for writing object
oriented code in C, and a high level set of API for streaming input/output,
file system access and enumeration, IPC, network programming, and system
integration.

## Components

GLib is composed by various libraries:

- **glib**: The basic, portable low level library providing data types,
  utilities, and main loop.
- **gobject**: The run time type system for object oriented programming.
- **gmodule**: A portable API for dynamically loading shared modules.
- **gio**: High and low level API for streaming I/O; file system access and
  enumeration; IPC; network programming; and system integration.

## Documentation

- [API reference for GLib](https://docs.gtk.org/glib/)
- [API reference for GModule](https://docs.gtk.org/gmodule/)
- [API reference for GObject](https://docs.gtk.org/gobject/)
- [API reference for GIO](https://docs.gtk.org/gio/)

## Sources

The GLib source is available on GNOME's GitLab instance at:
https://gitlab.gnome.org/GNOME/glib

You can download the release archives from:
https://download.gnome.org/sources/glib

## Reporting issues

If you have an issue using GLib, open a topic on GNOME's Discourse instance
under the ["glib" tag](https://discourse.gnome.org/tag/glib).

You can report bugs on the project's [GitLab issue
tracker](https://gitlab.gnome.org/GNOME/glib/-/issues).
