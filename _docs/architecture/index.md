---
---

# Overview of GTK and its Libraries

![GTK Architecture](/assets/img/docs/docs-gtk-architecture.svg)

Over time GTK has been built up to be based on various libraries, also
developed by the GTK team.

## GDK

GDK is the GTK windowing system abstraction; it provides the minimum common
API surface for writing GTK on top of various platforms, like Wayland and
X11 on Linux and Unix-like systems; Microsoft Windows; and Apple macOS.

## GSK

GSK is the GTK rendering abstraction; it provides a low level rendering API
to GTK in terms of draw operations, and converts them into commands for the
graphics pipeline.

## GLib

[GLib](https://www.gtk.org/docs/architecture/glib/) is a low-level core
library that forms the basis of GTK. It provides data structure handling for
C, portability wrappers and interfaces for such run-time functionality as an
event loop, threads, dynamic loading and an object system.

## Pango

[Pango](https://www.gtk.org/docs/architecture/pango/) is a library for
layout and rendering of text with an emphasis on internationalization. It
forms the core of text and font handling for GTK.

## GdkPixbuf

[GdkPixbuf](https://docs.gtk.org/gdk-pixbuf) is a library for loading
graphical assets like icons in various formats, like PNG, JPEG, and GIF.
GdkPixbuf is extensible through dynamically loaded modules, which can be
written and maintained independently from the main library.

## Cairo

[Cairo](https://www.cairographics.org/) is a library for 2D graphics with
support for multiple output devices (including the X Window System, Win32)
while producing a consistent output on all media while taking advantage of
display hardware acceleration when available.

## Graphene

[Graphene](https://github.com/ebassi/graphene) is a library providing
efficient types for 2D and 3D maths.

## HarfBuzz

[HarfBuzz](https://harfbuzz.github.io/) is a text shaping library. Using the
HarfBuzz library allows programs to convert a sequence of Unicode input into
properly formatted and positioned glyph output—for any writing system and
language. 
