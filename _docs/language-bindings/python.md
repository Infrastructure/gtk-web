---
---

# GTK and Python

## About

PyGObject is a Python package which provides bindings for GObject based
libraries such as GTK, GStreamer, WebKitGTK, GLib, GIO and many more.

It supports Linux, Windows, and macOS and works with Python 3.8+ and PyPy3.
PyGObject, including this documentation, is licensed under the LGPLv2.1+.

If you want to write a Python application for GNOME or a Python GUI
application using GTK, then PyGObject is the way to go. For tutorials please see the
[PyGObject Guide](https://rafaelmardojai.pages.gitlab.gnome.org/pygobject-guide)
and reference the [GNOME Python API Docs](https://amolenaar.pages.gitlab.gnome.org/pygobject-docs/).

For more information, visit the
[PyGObject Docs](https://gnome.pages.gitlab.gnome.org/pygobject/index.html).

## PyGObject API

You can view the API Reference for the PyGObject at the
[GNOME Python API Docs](https://amolenaar.pages.gitlab.gnome.org/pygobject-docs/).

## A Hello World app

Below is an `Hello World` program that can be used as an example to get
started with the PyGObject.

```python
import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

def on_activate(app):
    win = Gtk.ApplicationWindow(application=app)
    btn = Gtk.Button(label="Hello, World!")
    btn.connect('clicked', lambda x: win.close())
    win.set_child(btn)
    win.present()

app = Gtk.Application(application_id='org.gtk.Example')
app.connect('activate', on_activate)
app.run(None)
```

### Explanation

`gi` python module is imported to use APIs linked with the PyGObject.

## Contribute

If you are interested in contributing to the PyGObject project or want to get in touch with the original source files, you can visit the project's [git repository](https://gitlab.gnome.org/GNOME/pygobject/) on Gitlab.

## See More

* GitLab Project: [https://gitlab.gnome.org/GNOME/pygobject/](https://gitlab.gnome.org/GNOME/pygobject/)
* PyGObject Website: [https://gnome.pages.gitlab.gnome.org/pygobject](https://gnome.pages.gitlab.gnome.org/pygobject)
* API References: [https://amolenaar.pages.gitlab.gnome.org/pygobject-docs](https://amolenaar.pages.gitlab.gnome.org/pygobject-docs)
* PyGObject Guide: [https://rafaelmardojai.pages.gitlab.gnome.org/pygobject-guide](https://rafaelmardojai.pages.gitlab.gnome.org/pygobject-guide)
